import { printByte, printBuffer, readUInt16LE } from './utils'

const notes: string[] = ['c', 'c+', 'd', 'd+', 'e', 'f', 'f+', 'g', 'g+', 'a', 'a+', 'b']
const amkCurve = [0x19, 0x33, 0x4C, 0x66, 0x72, 0x7F, 0x8C, 0x99, 0xA5, 0xB2, 0xBF, 0xCC, 0xD8, 0xE5, 0xF2, 0xFC]
const qMap: number[] = []

type RenderOptions = {
  sequences: Map<number, number[][]>,
  paraList: number[],
  otherPointers: number[],
  absLen: boolean,
  percBegin: number,
  velTable: Uint8Array
}

export function render(options: RenderOptions) {
  const { sequences, paraList, otherPointers, absLen, percBegin, velTable } = options
  // 改写自 https://github.com/loveemu/spc_converters_legacy/blob/master/nintspc/src/nintspc.c
  function getNoteLenForMML(tick: number, division = 48) {
    if (absLen) {
      return `=${tick}`
    }
    const dotMax = 6
    const note = division * 4
    let l
    let dot
    let text = ''
    for (l = 1; l <= note; l += 1) {
      let cTick = 0
      for (dot = 0; dot <= dotMax; dot += 1) {
        const ld = (l << dot)
        if (note % ld) {
          break
        }
        cTick += note / ld
        if (tick === cTick) {
          text += l
          for (; dot > 0; dot -= 1) {
            text += '.'
          }
          return text
        }
      }
    }
    return `=${tick}`
  }

  velTable.forEach((e) => {
    const gaps = amkCurve.map((f, i) => {
      return {
        index: i,
        value: Math.abs(e - f)
      }
    })
    const sorted = gaps.sort((a, b) => {
      return a.value - b.value
    })
    qMap.push(sorted[0].index)
  })
  console.log(qMap)

  let label = 1
  const callID: { [key: number]: number | null; } = {}
  const usedInstLabels = new Map<string, number>()
  const usedPercLabels = new Map<string, number>()
  let usedInst: number[] = []
  otherPointers.forEach((e) => {
    callID[e] = null
  })
  function renderMML(
    sequence: number[][], handleSubroutine: boolean = false, channel: number = -1
  ) {
    const content: string[][] = []
    let current: string[] = []
    let prevOctave = 0
    let noteLength = 0
    let prevQ = 0
    let currentTotalTick = 0

    function add(e: string) {
      current.push(e)
    }

    function lineBreak() {
      if (current.length > 0) {
        content.push(current)
        current = []
      }
    }

    sequence.forEach((e, i) => {
      const h = e[0]
      const next = sequence[i + 1] || {}
      if (h >= 0x1 && h <= 0x7f) {
        noteLength = h
        if (e.length > 1) {
          const orig = e[1]
          const convertedVol = qMap[orig & 0xf]
          const converted = (orig & 0xf0) + convertedVol
          prevQ = converted
          add(`q${printByte(converted)} ; q${printByte(orig)}\n`)
        }
      } else if (h >= 0x80 && h <= 0xc7) {
        const note = h - 0x80
        const octave = Math.floor((h - 0x80) / 12) + 1
        if (prevOctave < 1) {
          add(`o${octave}`)
        } else if (octave > prevOctave) {
          add('>'.repeat(octave - prevOctave))
        } else if (octave < prevOctave) {
          add('<'.repeat(prevOctave - octave))
        }
        prevOctave = octave
        add(`${notes[note % 12]}${getNoteLenForMML(noteLength)}`)
        currentTotalTick += noteLength
        if (next[0] >= 0xca || currentTotalTick >= 192) {
          lineBreak()
        }
        if (currentTotalTick >= 192) {
          currentTotalTick = 0
        }
      } else if (h === 0xc8) {
        add(`^${getNoteLenForMML(noteLength)}`)
        currentTotalTick += noteLength
        if (next[0] >= 0xca || currentTotalTick >= 192) {
          lineBreak()
        }
        if (currentTotalTick >= 192) {
          currentTotalTick = 0
        }
      } else if (h === 0xc9) {
        add(`r${getNoteLenForMML(noteLength)}`)
        currentTotalTick += noteLength
        if (next[0] >= 0xca || currentTotalTick >= 192) {
          lineBreak()
        }
        if (currentTotalTick >= 192) {
          currentTotalTick = 0
        }
      } else if (h >= 0xca && h < 0xd4) {
        const n = `PERC_${printByte(h)}X`
        usedPercLabels.set(n, h - 0xca + percBegin)
        usedInst.push(h - 0xca + percBegin)
        add(`${n}${getNoteLenForMML(noteLength)}`)
        currentTotalTick += noteLength
        if (next[0] >= 0xd4 || currentTotalTick >= 192) {
          lineBreak()
        }
        if (currentTotalTick >= 192) {
          currentTotalTick = 0
        }
      } else {
        switch (h) {
          // set instrument
          case 0xd5: {
            lineBreak()
            const name = `INST_${printByte(e[1])}`
            usedInstLabels.set(name, e[1])
            usedInst.push(e[1])
            add(name)
            break
          }
          // handle subroutine
          case 0xd6:
            if (handleSubroutine) {
              const addr = readUInt16LE(e, 1)
              lineBreak()
              let loopCall = ''
              if (callID[addr] === null) {
                callID[addr] = label
                label += 1
                loopCall = `[\n${renderMML(sequences.get(addr) as number[][])}\n]`
              }
              loopCall = `(${callID[addr]})${loopCall}${e[3]}`
              add(loopCall)
              lineBreak()
              prevOctave = 0
            }
            break
          // set volume
          case 0xd7:
            add(`v${e[1]}`)
            break
          // set adsr
          case 0xd8:
            add(`$ED $${printByte(e[1] - 0x80)} $${printByte(e[2])}`)
            break
          // set panpot
          case 0xd9:
            if (e[1] <= 20) {
              add(`y${e[1]}`)
            } else {
              const echoL = (e[1] >> 7) % 2
              const echoR = (e[1] >> 6) % 2
              add(`y${e[1] % 0x40},${echoL},${echoR}`)
            }
            break
          // handle software tuning
          case 0xdc:
            add(`$FA $02 $${printByte(e[1])}`)
            break
          // handle restart pos
          case 0xdd:
            lineBreak()
            add('/')
            lineBreak()
            break
          // track end
          case 0xde:
            break
          // set tempo
          case 0xdf:
            add(`t${e[1]}`)
            break
          // set pitch bend?
          case 0xe0:
            add(`$DD ${printBuffer(e.slice(1))}`)
            break
          // set vibrato
          case 0xe1:
            add(`$DE ${printBuffer(e.slice(1))}`)
            break
          // set vec
          case 0xe7:
          case 0xe8:
          case 0xe9:
          case 0xea:
          case 0xeb:
          case 0xec:
          case 0xed:
          case 0xee:
          case 0xef:
          case 0xf0:
          case 0xf1:
          case 0xf2:
          case 0xf3:
          case 0xf4:
          case 0xf5: {
            const orig = (prevQ & 0xf0) + e[0] - 0xe6
            const convertedVol = qMap[orig & 0xf]
            add(`q${printByte((orig & 0xf0) + convertedVol)} ; q${printByte(orig)}\n`)
            break
          }
          // handle echo param 1
          case 0xf6:
            add(`$EF ${printBuffer(e.slice(1))}`)
            break
          // handle echo param 2
          case 0xf8:
            add(`$F1 ${printBuffer(e.slice(1))}`)
            break
          // unknown cmds
          default:
            add(`; ${printBuffer(e)}`)
            lineBreak()
        }
      }
    })
    if (current.length > 0) {
      content.push(current)
    }
    const finalPrint: string[] = []
    content.forEach((e) => {
      finalPrint.push(e.join(' '))
    })
    return finalPrint.join('\n')
  }
  let mml = ''
  for (let i = 0; i < 8; i++) {
    if (paraList[i] !== 0) {
      mml += `#${i}\n`
      mml += renderMML(sequences.get(paraList[i]) as number[][], true, i)
      mml += '\n\n'
    }
  }
  usedInst = Array.from(new Set(usedInst))
  usedInstLabels.forEach((k, v) => {
    mml = `"${v}=@${usedInst.findIndex((f) => f === k) + 30}"\n` + mml
  })
  usedPercLabels.forEach((k, v) => {
    mml = `"${v}=@${usedInst.findIndex((f) => f === k) + 30} o4 c"\n` + mml
  })
  return { mml, usedInst }
}
