import minimist from 'minimist'
import fs from 'fs-extra'
import path from 'path'
import { parse } from './parse'

const argv = minimist(process.argv.slice(2))

const input: string = argv._[0]
const output: string = argv._[1] ?? input + '_out'
const entryPoint: number = Number(argv.entrypoint ?? argv.e ?? 0x90)
const percBegin: number = Number(argv.percbegin ?? argv.p ?? 0x14)
const instListPtr: number = Number(argv.instlist ?? argv.y ?? 0x3c00)

function showHelp() {
  process.stdout.write(`Usage: suemura-parser [options...] <input> [output]
 -e, --entrypoint   Set song entry point. Default: 0x90
 -p, --percbegin    Set which sample the percussion begins. Default: 0x14
 -y, --instlist     Set instrument list pointer. Default: 0x3D00
`)
  process.exit(1)
}

if (!input) {
  showHelp()
}

const data = fs.readFileSync(input as string)
const files = parse(data, {
  logger: console.log,
  entryPoint,
  percBegin,
  instListPtr
})
fs.mkdirpSync(output)
files.forEach((e, name) => {
  const realName = name.split('/')
  if (realName.length > 1) {
    fs.mkdirpSync(path.join(output, ...realName.slice(0, -1)))
  }
  if (typeof e === 'string') {
    fs.writeFileSync(path.join(output, ...realName), e, { encoding: 'utf8' })
    return
  }
  fs.writeFileSync(path.join(output, ...realName), e)
})
