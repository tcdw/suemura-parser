import { readUInt16LE } from './utils'

interface ParseResult {
    content: number[][];
    jumps: number[];
}

export function parseSeq(aram: Uint8Array, beginPointer: number): ParseResult {
  const vcmdStart = 0xd5
  const vcmdLength = [
    0x01, 0x03, 0x01, // $D5 - $D7
    0x02, 0x01, 0x02, 0x01, 0x01, 0x00, 0x00, 0x01, // $D8 - $DF
    0x03, 0x03, 0x00, 0x01, 0x01, 0x01, 0x00, 0x00, // $E0 - $E7
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // $E8 - $EF
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, // $F0 - $F7
    0x03, 0x00, 0x00, 0x02, 0x01, 0x00 // $F8 - $FD
  ]
  const content: number[][] = []
  const jumps: number[] = []
  let nowPointer = beginPointer
  let shouldBreakLater = false
  while (true) {
    const now = aram[nowPointer]
    // note length
    if (now === 0x0) {
      break
    } else if (now >= 0x1 && now <= 0x7f) {
      const temp: number[] = []
      temp.push(now)
      // duration rate & velocity rate
      if (aram[nowPointer + 1] >= 0x1 && aram[nowPointer + 1] <= 0x7f) {
        temp.push(aram[nowPointer + 1])
        nowPointer += 1
      }
      content.push(temp)
      nowPointer += 1
      // note
    } else if (now >= 0x80 && now <= 0xc9) {
      content.push([now])
      nowPointer += 1
      // percussion
    } else if (now >= 0xca && now <= 0xd4) {
      content.push([now])
      nowPointer += 1
      // vcmd
    } else if (now >= vcmdStart && now <= 0xff) {
      const len = vcmdLength[now - vcmdStart] + 1

      // special: d6 [xx yy] zz
      if (now === 0xd6) {
        jumps.push(readUInt16LE(aram, nowPointer + 1))
      }

      if (now === 0xde) {
        shouldBreakLater = true
      }

      const temp = new Uint8Array(len)
      temp.set(aram.slice(nowPointer, nowPointer + len))
      content.push(Array.from(temp))
      nowPointer += len
      if (shouldBreakLater) {
        break
      }
    } else {
      throw new Error(`Unexpected command 0x${now.toString(16)} at 0x${nowPointer.toString(16)}`)
    }
    // console.log(nowPointer.toString(16) + ": " + content[content.length - 1]);
  }
  return { content, jumps }
}
