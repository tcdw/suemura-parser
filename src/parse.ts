// dur: \x2d\x9f\x28\x07\xfd\xf6..\xd5..\xae\x28\x0f\xfd\xf6..\xd5..

import { finalize } from './finalize'
import { parseSeq } from './parse-seq'
import { render } from './render-mml'
import SPCFile from './spc-file'
import { fineHex, searchPattern, readUInt16LE } from './utils'

export type ParseOptions = {
  entryPoint: number
  percBegin: number
  instListPtr: number
  logger: (...msg: string[]) => void
}

export function parse(spcData: Uint8Array, options: ParseOptions) {
  const { percBegin, entryPoint, instListPtr, logger } = options
  const spc = new SPCFile(spcData)
  const tracks: number[] = []
  const trackPtr = readUInt16LE(spc.aram, entryPoint)
  logger(`Entry Point: ${fineHex(trackPtr)}`)

  const pos1 = searchPattern(spc.aram, [[0x2d, 0x9f, 0x28, 0x07, 0xfd, 0xf6], 2, [0xd5], 2, [0xae, 0x28, 0x0f, 0xfd, 0xf6], 2, [0xd5], 2])
  if (pos1 === null) {
    throw new Error('Unable to find note parameter table')
  }
  const durTableAddr = readUInt16LE(spc.aram, pos1 + 6)
  const velTableAddr = readUInt16LE(spc.aram, pos1 + 16)
  const durTable = spc.aram.slice(durTableAddr, durTableAddr + 8)
  const velTable = spc.aram.slice(velTableAddr, velTableAddr + 16)

  for (let i = 0; i < 8; i++) {
    const ptr = readUInt16LE(spc.aram, i * 2 + trackPtr)
    logger(`Track ${i}: ${fineHex(i * 2 + trackPtr)} => ${fineHex(ptr)}`)
    tracks.push(ptr)
  }

  const seqs = new Map<number, number[][]>()
  let otherPointers: number[] = []
  tracks.forEach((e) => {
    const seq = parseSeq(spc.aram, e)
    seqs.set(e, seq.content)
    otherPointers.push(...seq.jumps)

    seq.jumps.forEach((f) => {
      const subSeq = parseSeq(spc.aram, f)
      seqs.set(f, subSeq.content)
      otherPointers.push(...subSeq.jumps)
    })
  })
  otherPointers = Array.from(new Set(otherPointers))

  const { mml, usedInst } = render({
    sequences: seqs,
    paraList: tracks,
    otherPointers,
    absLen: false,
    percBegin,
    velTable
  })
  const files = finalize({
    usedInst,
    mml,
    spc,
    instListPtr
  })
  return files
}
