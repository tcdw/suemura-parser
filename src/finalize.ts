import SPCFile from './spc-file'
import { printBuffer, printByte } from './utils'

type FinalizeOptions = {
  usedInst: number[]
  mml: string
  spc: SPCFile
  instListPtr: number
}

type FileTypes = Uint8Array | string

export function finalize(options: FinalizeOptions) {
  const files = new Map<string, FileTypes>()
  const { usedInst, mml, spc, instListPtr } = options
  const sampleDirName = `${+new Date()}`
  let header = `#amk 2
#path "${sampleDirName}"
#samples
{
\t#optimized
`
  usedInst.forEach((e) => {
    header += `\t"${printByte(e)}.brr"\n`
    files.set(`${sampleDirName}/${printByte(e)}.brr`, spc.getBRR(e))
  })
  header += `}
#instruments
{
`
  usedInst.forEach((e) => {
    const cut = spc.aram.slice(instListPtr + e * 6 + 1, instListPtr + (e + 1) * 6)
    header += `\t"${printByte(e)}.brr" ${printBuffer(cut)}\n`
  })
  header += '}\n#spc\n{'
  const textDecoder = new TextDecoder('utf-8')
  let spcAuthor = textDecoder.decode(spc.mainHeader.slice(0xB1, 0xD1))
  let spcGame = textDecoder.decode(spc.mainHeader.slice(0x4E, 0x6E))
  let spcTitle = textDecoder.decode(spc.mainHeader.slice(0x2E, 0x4E))
  if (spcAuthor.indexOf('\0') >= 0) {
    spcAuthor = spcAuthor.slice(0, spcAuthor.indexOf('\0'))
  }
  if (spcGame.indexOf('\0') >= 0) {
    spcGame = spcGame.slice(0, spcGame.indexOf('\0'))
  }
  if (spcTitle.indexOf('\0') >= 0) {
    spcTitle = spcTitle.slice(0, spcTitle.indexOf('\0'))
  }
  header += `
\t#author    "${spcAuthor}"
\t#game      "${spcGame} / SMW"
\t#comment   ""
\t#title     "${spcTitle}"
}
`

  const finalMML = header + mml
  files.set(`${spcGame} - ${spcTitle}.txt`, finalMML)
  return files
}
